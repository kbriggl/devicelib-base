#ifndef VPowerSupply_h
#define VPowerSupply_h

#include <string>

using namespace std;

class VPowerSupply
{
	public:

		VPowerSupply(){};
		virtual ~VPowerSupply(){};

		virtual int Setcom( const char* port ) = 0;
		virtual void Init() = 0;
		virtual void On() = 0;
		virtual void Off() = 0;
		virtual void RampOn( float v ) = 0;
		//virtual void ProtClr() = 0;
		virtual void SetIlim( float clim ) = 0;
		virtual void SetVolt( float volt ) = 0;
		//virtual void SetOCP( bool on ) = 0;
		//virtual void SetOVP( float volt ) = 0;
		virtual double GetVolt() = 0;
		virtual double MeasVolt() = 0;
		virtual double MeasCurrent(){return -1;};

		///not working yet!!!
		//virtual void Text( char* text ) = 0;
		//virtual void DispMode( char* mode ) = 0;

	protected:

		string com;
		char cmd[128];
};

#endif

