/*
 * SCPIoUART: Base Class for SCPI communication with some device connected to a UART
 *
 *  Created on: 19.11.2016
 *      Author: K.Briggl
 */

#ifndef __SCPIoUART_H
#define __SCPIoUART_H

#include<termios.h>
#include<string>
#include<sstream>
#include<list>
#include<vector>

class SCPIoUART{
	private:
		char m_cmd_delim;
		char m_reply_delim;

		struct termios m_oldtio,m_options;
		std::stringstream m_inbufstream;
		void setNewOptions(int baudrate, int databits, const std::string& parity, const std::string& stop, bool softwareHandshake, bool hardwareHandshake);

	protected:
		int m_fd;
	public:
		SCPIoUART(const char *devname="",int baudrate=9600, char cmd_delim='\n', char reply_delim='\n');
		~SCPIoUART();

		virtual int Setcom( const char* devname, int baudrate);

		//Write SCPI command, no reply
		virtual int Command(std::string cmdstring, ...);

		//Write SCPI command, read reply:
		//	one line of reply as string, delimited by newline (via std::getline([],[]))
		virtual int CommandReply(std::string cmdstring, std::string& reply, ...);

		//Poll until n lines have been finished reading. Reset vector first. Return -1 on error, 0 otherwise
		virtual int ReadReplyLines(std::vector<std::string>& reply, int n);

		//Poll until one line has been finished reading. Return -1 on error, 0 otherwise 
		virtual int ReadReplyLine(std::string& reply);

		//If an additional line is in the reply buffer, return it. Return -1 if no lines are available, 0 otherwise
		virtual int GetReplyLine(std::string& reply);

		//Clear local read buffer, read from device until EAGAIN
		virtual void FlushBuffer(int min_read=0);


		//Write SCPI command, read reply:
		//	multiple lines, delimited by newline (via std::getline([],[],line_delim)). Store in vector. Read at least nline strings 

		//Write SCPI command, read reply:
		//	one line of reply as string, delimited by newline (via std::getline([],[])), then parse:
		//	list of entries of type <T>, delimited by delim (via std::getline([],[],delim)).
		//	Parse using sscanf([],parse_str,&item); Store each entry in list
		template <class T_item> int CommandReply(std::string cmdstring, std::list<T_item>& result, char delim, const char* parse_str, ...);
		template <class T_item> T_item CommandReply(std::string cmdstring, const char* parse_str, ...);

		//Write SCPI command, read list of replies using the above method as Command<double>(cmdstring,delim,parse_str).
		//Calculate mean and standard deviation from results list
		virtual int CommandReply(std::string cmdstring, double& mu, double& sdev, char delim=',', const char* parse_str="%le", ...);
};
#include "SCPIoUART.hpp"

#endif
