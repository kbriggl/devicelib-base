//Implementation of template member functions for the SCPIoUART class
//
//

#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<stdlib.h>
#include<iostream>
#include<sstream>
#include<list>
#include<math.h>
#include <stdarg.h>


template <class T_item> int SCPIoUART::CommandReply(std::string cmdstring, std::list<T_item>& result, char delim, const char* parse_str, ...){
	//printf("SCPIoUART::CommandReply<>(&list)\n");
	std::string resstr;

	va_list argptr;
	va_start(argptr, parse_str);
	if(CommandReply(cmdstring, resstr, argptr)<0) return -1;
	va_end(argptr);

	//printf("SCPIoUART::CommandReply<>(&list): Now parsing\n");
	std::stringstream ss(resstr);
	//fill list of results
	int n=0;
	std::string item_str;
	while(std::getline(ss,item_str,',')){
		T_item item;
		if(sscanf(item_str.c_str(),parse_str,&item)!=1){
			std::cout<<"Error in item: "<<item_str<<std::endl;
			continue;
		}
		n++;
		result.push_back(item);
	}
	return n;
}
template <class T_item> T_item SCPIoUART::CommandReply(std::string cmdstring, const char* parse_str, ...){
	std::list<T_item> reslist;
	va_list argptr;
	va_start(argptr, parse_str);
	int n=CommandReply<T_item>(cmdstring,reslist,',',parse_str,argptr);
	printf("SCPIoUART::CommandReply<>(): Got %d entries.\n",n);
	if(n<=0) return -1;
	va_end(argptr);
	return reslist.front();
}


