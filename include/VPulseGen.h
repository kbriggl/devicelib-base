#ifndef VPulseGen_h
#define VPulseGen_h

class VPulseGen
{
	public:
		VPulseGen(){};
		virtual ~VPulseGen(){};

		virtual void On() = 0;
		virtual void Off() = 0;
		virtual void Setcom( const char* port )= 0;

	protected:

};

#endif

