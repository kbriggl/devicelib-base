/*
 * SCPIoTCP: Base Class for SCPI communication with some device connected via TCP
 *
 *  Created on: 20.04.2016
 *      Author: D.Schimansky
 */

#ifndef __SCPIoTCP_H
#define __SCPIoTCP_H

#include<termios.h>
#include<string>
#include<sstream>
#include<list>
#include<vector>

#include "SCPIoUART.h"


class SCPIoTCP: public SCPIoUART{


	private:

		//members for tcp operations
		const char* m_host;
		short unsigned int m_port;
	
		//connect if unconnected or try reconnect
		//register client for given handleID
		void Connect();
		int Socket(bool retry=true);


//		int Setcom( const char* devname, int baudrate=9600);
	public:
		SCPIoTCP(const char *host="", unsigned short port=23, char cmd_delim='\n', char reply_delim='\n');
		~SCPIoTCP();
};

#endif
