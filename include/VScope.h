#ifndef VScope_h
#define VScope_h

#include "string"
#include "TGraph.h"
#include "TCanvas.h"

using namespace std;

class TGraph;

class VScope
{
	public:

		typedef enum
		{
			AC1M,
			DC1M,
			DC50
		}Vcoupling;

		typedef enum
		{
			RISING,
			FALLING
		}VTrigType;

		VScope(int nchannels);
		virtual ~VScope();

		virtual int OpenDevice( string address = "" ) = 0;
		virtual int CloseDevice() = 0;
		virtual int EnableChannel( int channel, bool enable = true ) = 0;
		virtual int SetCoupling( int channel, Vcoupling coupling ) = 0;
		virtual int SetVScale( int channel, float mV, float offset = 0 ) = 0;
		virtual int SetTScale( float ns, float gsps, float offset = 0 ) = 0;
		virtual int SetTriggerSimple( int channel, float threshold, VTrigType type = RISING ) = 0;
		virtual int SetTriggerCoin( int channel1, int channel2, float threshold1, float threshold2 ) = 0;
		virtual int SetNSequence( int nSeq ) = 0;
		virtual int Arm() = 0;
		virtual int Force() = 0;
		virtual int Stop() = 0;
		virtual TGraph* GetWaveform( int channel ) = 0;
		virtual void WriteBuffer( const char* filename, bool append=true ) = 0;
		virtual float GetVMin() = 0;
		virtual float GetVMax() = 0;
		virtual float GetVRes() = 0;
		virtual int GetNSamples() = 0;
		virtual int GetNSamplesOnScreen() = 0;
		virtual int GetNSequence() = 0;
		virtual char*		Command( string cmd, unsigned long timeout = 10000 ){return (char* const)"not_implemented";};

		string GetName(){ return dev_name; };

		virtual void		EnableMonitor( bool on );
		virtual void		ResetMonitor();

		//set monitor y range:
		//if min==max: autoscale
		//if min!=max && fix : force this setting on every update
		//if min!=max && !fix : set this setting once
		virtual void		SetMonitorYRange(float min, float max,bool fix);

	protected:
		const int nchan;
		TGraph**			g_wf;
		TCanvas			*c_mon;
		bool 			monitoring_enabled;
		virtual void		UpdateMonitor();
		float monitor_ymin,monitor_ymax;
		bool monitor_yauto;
		bool monitor_yfixed;

		string dev_name;

};

#endif

