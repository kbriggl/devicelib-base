/*
 * SCPIoUART Implementation: Base Class for SCPI communication with some device connected to a UART
 *
 *  Created on: 19.11.2016
 *      Author: K.Briggl
 */


#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<termios.h>
#include<stdlib.h>
#include "malloc.h"
#include <sys/ioctl.h>
#include<iostream>
#include<sstream>
#include<list>
#include<math.h>
#include <stdarg.h>

#include "SCPIoUART.h"

using namespace std;

//#define DEBUG

#ifdef DEBUG
	#define dprintf(...) printf (__VA_ARGS__)
#else
	#define dprintf(...) 
#endif

#define MAX_REPLY_LEN 64

/*Function code taken from Cutecom sources*/
void SCPIoUART::setNewOptions(int baudrate, int databits, const std::string& parity, const std::string& stop, bool softwareHandshake, bool hardwareHandshake)
{
   struct termios newtio;
//   memset(&newtio, 0, sizeof(newtio));
   if (tcgetattr(m_fd, &newtio)!=0)
   {
      std::cerr<<"tcgetattr() 3 failed"<<std::endl;
   }

   speed_t _baud=0;
   switch (baudrate)
   {
#ifdef B0
   case      0: _baud=B0;     break;
#endif
#ifdef B50
   case     50: _baud=B50;    break;
#endif
#ifdef B75
   case     75: _baud=B75;    break;
#endif
#ifdef B110
   case    110: _baud=B110;   break;
#endif
#ifdef B134
   case    134: _baud=B134;   break;
#endif
#ifdef B150
   case    150: _baud=B150;   break;
#endif
#ifdef B200
   case    200: _baud=B200;   break;
#endif
#ifdef B300
   case    300: _baud=B300;   break;
#endif
#ifdef B600
   case    600: _baud=B600;   break;
#endif
#ifdef B1200
   case   1200: _baud=B1200;  break;
#endif
#ifdef B1800
   case   1800: _baud=B1800;  break;
#endif
#ifdef B2400
   case   2400: _baud=B2400;  break;
#endif
#ifdef B4800
   case   4800: _baud=B4800;  break;
#endif
#ifdef B7200
   case   7200: _baud=B7200;  break;
#endif
#ifdef B9600
   case   9600: _baud=B9600;  break;
#endif
#ifdef B14400
   case  14400: _baud=B14400; break;
#endif
#ifdef B19200
   case  19200: _baud=B19200; break;
#endif
#ifdef B28800
   case  28800: _baud=B28800; break;
#endif
#ifdef B38400
   case  38400: _baud=B38400; break;
#endif
#ifdef B57600
   case  57600: _baud=B57600; break;
#endif
#ifdef B76800
   case  76800: _baud=B76800; break;
#endif
#ifdef B115200
   case 115200: _baud=B115200; break;
#endif
#ifdef B128000
   case 128000: _baud=B128000; break;
#endif
#ifdef B230400
   case 230400: _baud=B230400; break;
#endif
#ifdef B460800
   case 460800: _baud=B460800; break;
#endif
#ifdef B576000
   case 576000: _baud=B576000; break;
#endif
#ifdef B921600
   case 921600: _baud=B921600; break;
#endif
   default:
//   case 256000:
//      _baud=B256000;
      break;
   }
   cfsetospeed(&newtio, (speed_t)_baud);
   cfsetispeed(&newtio, (speed_t)_baud);

   /* We generate mark and space parity ourself. */
   if (databits == 7 && (parity=="Mark" || parity == "Space"))
   {
      databits = 8;
   }
   switch (databits)
   {
   case 5:
      newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS5;
      break;
   case 6:
      newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS6;
      break;
   case 7:
      newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS7;
      break;
   case 8:
   default:
      newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS8;
      break;
   }
   newtio.c_cflag |= CLOCAL | CREAD;

   //parity
   newtio.c_cflag &= ~(PARENB | PARODD);
   if (parity == "Even")
   {
      newtio.c_cflag |= PARENB;
   }
   else if (parity== "Odd")
   {
      newtio.c_cflag |= (PARENB | PARODD);
   }

   //hardware handshake
/*   if (hardwareHandshake)
      newtio.c_cflag |= CRTSCTS;
   else
      newtio.c_cflag &= ~CRTSCTS;*/
   newtio.c_cflag &= ~CRTSCTS;

   //stopbits
   if (stop=="2")
   {
      newtio.c_cflag |= CSTOPB;
   }
   else
   {
      newtio.c_cflag &= ~CSTOPB;
   }

//   newtio.c_iflag=IGNPAR | IGNBRK;
   newtio.c_iflag=IGNBRK;
//   newtio.c_iflag=IGNPAR;

   //software handshake
   if (softwareHandshake)
   {
      newtio.c_iflag |= IXON | IXOFF;
   }
   else
   {
      newtio.c_iflag &= ~(IXON|IXOFF|IXANY);
   }

   newtio.c_lflag=0;
   newtio.c_oflag=0;

   newtio.c_cc[VTIME]=1;
   newtio.c_cc[VMIN]=60;

//   tcflush(m_fd, TCIFLUSH);
   if (tcsetattr(m_fd, TCSANOW, &newtio)!=0)
   {
      std::cerr<<"tcsetattr() 1 failed"<<std::endl;
   }

   int mcs=0;
   ioctl(m_fd, TIOCMGET, &mcs);
   mcs |= TIOCM_RTS;
   ioctl(m_fd, TIOCMSET, &mcs);

   if (tcgetattr(m_fd, &newtio)!=0)
   {
      std::cerr<<"tcgetattr() 4 failed"<<std::endl;
   }

   //hardware handshake
   if (hardwareHandshake)
   {
      newtio.c_cflag |= CRTSCTS;
   }
   else
   {
      newtio.c_cflag &= ~CRTSCTS;
   }
/*  if (on)
     newtio.c_cflag |= CRTSCTS;
  else
     newtio.c_cflag &= ~CRTSCTS;*/
   if (tcsetattr(m_fd, TCSANOW, &newtio)!=0)
   {
      std::cerr<<"tcsetattr() 2 failed"<<std::endl;
   }

}

int SCPIoUART::Setcom(const char *devname, int baudrate)
{
	if(strlen(devname)==0){
		m_fd = -1;
		return -1;
	}
	//try to open device
	//    printf("Trying to open device \"%s\"\n",deviceName);
	m_fd = open(devname, O_RDWR | O_NDELAY) ;
	if(m_fd<0)
	{
	perror("Open");
	m_fd=-1;
	return -1;
	}
	// flushing is to be done after opening. This prevents first read and write to be spam'ish.
	tcflush(m_fd, TCIOFLUSH);


	int n = fcntl(m_fd, F_GETFL, 0);
	fcntl(m_fd, F_SETFL, n & ~O_NDELAY);

	if (tcgetattr(m_fd, &m_oldtio)!=0)
	{
		std::cerr<<"tcgetattr() 2 failed"<<std::endl;
	}

	setNewOptions(baudrate, 8,"none","1",false,false);

	return 0;
}


SCPIoUART::SCPIoUART(const char *devname, int baudrate, char cmd_delim, char reply_delim)
{
	m_cmd_delim = cmd_delim;
	m_reply_delim = reply_delim;

	Setcom(devname,baudrate);
}

SCPIoUART::~SCPIoUART()
{
	if (m_fd!=-1){
		tcsetattr(m_fd, TCSANOW, &m_oldtio);
	}
	close(m_fd);
}

int SCPIoUART::Command(std::string cmdstring, ...)
{
	if (m_fd<0) return -1;
	//printf("SCPIoUART::Command()\n");
	char buffer[1024];
	va_list argptr;
	va_start(argptr, cmdstring);
	cmdstring+=m_cmd_delim;
	int status = vsnprintf(buffer,1024, cmdstring.c_str(), argptr);
	if(status>1023){
		fprintf(stderr,"Error: Command string truncated.\n");
		return -1;
	}
	dprintf("Writing: \"%s\"\n",buffer);
	status=write(m_fd,buffer,status);
	va_end(argptr);
	if(status==-1){
		fprintf(stderr,"Error writing to Serial device.\n");
	}
	//printf("Status: %d\n",status);
	return status;
}



int SCPIoUART::CommandReply(std::string cmdstring, std::string& reply, ...){
	if (m_fd<0) return -1;
	dprintf("SCPIoUART::CommandReply()\n");
	va_list argptr;
	va_start(argptr, reply);
	if(Command(cmdstring, argptr)<0) return -1;
	va_end(argptr);

	return ReadReplyLine(reply);
}

//Fetch from buffer
int SCPIoUART::GetReplyLine(std::string& reply){
	reply="";
	std::getline(m_inbufstream,reply,m_reply_delim);
	if(!m_inbufstream.good()){
		//not enough data...
		//undo last get, we want to read this later
		if(reply.length()!=0){ for(unsigned int i=0;i<reply.length();i++) m_inbufstream.rdbuf()->sungetc(); }
		return -1;
	}else{
		while(reply.c_str()[reply.length()-1]==m_reply_delim){reply.erase(reply.length()-1,reply.length()-1);};
		dprintf("SCPIoUART::GetReplyLine(): Line finished\n");
		dprintf("SCPIoUART::GetReplyLine(): Line read: \"%s\"\n",reply.c_str());
		return 0;
	}
} 

//Fill buffer, fetch line
int SCPIoUART::ReadReplyLine(std::string& reply){
	dprintf("SCPIoUART::ReadReplyLine()\n");
	tcdrain(m_fd);
	char buffer[64];
	while(1){
		//get reply line (first try in buffer), continue reading if no line available, otherwise return
		if(GetReplyLine(reply)==0)
			return 0;

		//try to read from device
		int status=read(m_fd,buffer,63);
		if (status<0)
		{
			if(errno==EAGAIN){
				usleep(1);
				continue;
			}
			fprintf(stderr,"Error reading all bytes from Device. Status=%d\n",status);
			perror("Read:");
			return -1;
		}
		dprintf("Read %d bytes...\n",status);
		//put in buffer, extract one line. Clear error flags before
		m_inbufstream.clear();
		m_inbufstream.write(buffer,status);

		buffer[status]='\0';
		dprintf("Data: \n%s\n",buffer);

	}
}

//Flush device read buffer
void SCPIoUART::FlushBuffer(int min_read){
	dprintf("SCPIoUART::FlushBuffer()\n");
	tcdrain(m_fd);
	m_inbufstream.str("");
	m_inbufstream.clear();
	char buffer[64];
	int n=0;
	int fl = fcntl(m_fd, F_GETFL, 0);
	fcntl(m_fd, F_SETFL, fl | O_NDELAY);
	while(1){
		//try to read from device
		int status=read(m_fd,buffer,63);
		dprintf("Read %d bytes...\n",status);
		if (status<0){
			if(errno!=EAGAIN){ return; } //io error
		}else
			n+=status; //no error

		if (status==63) continue; //potentially more to read
		if (n>=min_read) return;
	}
	fcntl(m_fd, F_SETFL, fl & ~O_NDELAY);
}


//Read & Parse n lines
int SCPIoUART::ReadReplyLines(std::vector<std::string>& reply, int n){
	dprintf("SCPIoUART::ReadReplyLines()\n");
	reply.clear();
	std::string line;
	while(n>0){
		if(ReadReplyLine(line)!=-1){
			reply.push_back(line);
			dprintf("SCPIoUART::ReadReplyLines(): %d\n",n);
			n--;
		}
		else return -1;
	}
	return 0;
}

int SCPIoUART::CommandReply(std::string cmdstring, double& mu, double& sdev, char delim, const char* parse_str, ...){
	dprintf("SCPIoUART::CommandReply(&mu,&sigma)\n");
	mu=0;
	sdev=0;
	std::list<double> reslist;

	va_list argptr;
	va_start(argptr, parse_str);
	int n=CommandReply<double>(cmdstring,reslist,delim,parse_str,argptr);
	if(n<=0) return n;
	va_end(argptr);

	//calculate mean
	for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
		mu+=*it;
	}
	mu/=reslist.size();
	//calculate sdev
	for(std::list<double>::iterator it=reslist.begin();it!=reslist.end();it++){
		sdev+=(*it-mu)*(*it-mu);
	}
	sdev/=reslist.size();
	sdev=sqrt(sdev);
	return n;
}
