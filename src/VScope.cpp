#include "VScope.h"
#include "TString.h"
#include "TAxis.h"
#include "TList.h"
VScope::VScope(int nchannels):nchan(nchannels)
{
	g_wf=new TGraph*[nchannels];
	for (int i=0;i<nchannels;i++){
		g_wf[i]=new TGraph(0);
		g_wf[i]->SetNameTitle(TString::Format("ch%d",i+1),TString::Format("Channel %d",i+1));
	}
	c_mon = NULL;
	monitoring_enabled=false;
}

VScope::~VScope()
{
	for (int i=0;i<nchan;i++)
		delete g_wf[i];	
	if(c_mon != NULL) delete c_mon;
}


void VScope::EnableMonitor( bool on )
{
	if(on==true){
		monitoring_enabled=true;
		if(c_mon==NULL){
			c_mon = new TCanvas("Le[croy|pico]Monitor","Le[croy|pico]Monitor",0,0,700,500);
			const int colours[]={4,2,3,5};
			for (int channel=0;channel<nchan;channel++){
				if(channel<4)
					g_wf[channel]->SetLineColor(colours[channel]);
			}
		}
	}else
	{	//do not delete the monitor, just no update
		//delete c_mon;
		//c_mon=NULL;
		if(c_mon!=NULL){
			c_mon->Clear();
		}
		
		monitoring_enabled=false;
	}
}

void VScope::ResetMonitor()
{
	if(c_mon!=NULL)
		c_mon->Clear();
	for (int i=0;i<nchan;i++)
		g_wf[i]->Set(0);
}

void VScope::SetMonitorYRange(float min, float max,bool fix){
	monitor_yauto=(monitor_ymin==monitor_ymax);
	monitor_yfixed=fix;
	if(c_mon==NULL)
		return;
	if(!monitor_yauto){
		monitor_ymin=min;
		monitor_ymax=max;
		for (int channel=0;channel<nchan;channel++){
			if(g_wf[channel]->GetYaxis())
				g_wf[channel]->GetYaxis()->SetRangeUser(min,max);
		}
	}
}

void VScope::UpdateMonitor(){
	if(c_mon==NULL || monitoring_enabled==false)
		return;
	c_mon->cd();
	double yrange[2]={1e9,-1e9};
	if(monitor_yauto) 
		for (int channel=0;channel<nchan;channel++){
			if(g_wf[channel]->GetN()>0){
				if(g_wf[channel]->GetMaximum()>yrange[1]) yrange[1]=g_wf[channel]->GetMaximum();
				if(g_wf[channel]->GetMinimum()<yrange[0]) yrange[0]=g_wf[channel]->GetMinimum();
			}
		}
	else{
		yrange[0]=monitor_ymin;
		yrange[1]=monitor_ymax;
	}
	for (int channel=0;channel<nchan;channel++){
		if(g_wf[channel]->GetN()>0){
			c_mon->cd(channel+1);
			if(c_mon->GetListOfPrimitives()->FindObject(TString::Format("ch%d",channel+1))==NULL)
				g_wf[channel]->Draw((c_mon->GetListOfPrimitives()->GetSize()==0)?"ALP":"SAMELP");
			if((monitor_yauto) || monitor_yfixed)
				g_wf[channel]->GetYaxis()->SetRangeUser(yrange[0],yrange[1]);
		}
	}
	c_mon->Update();
	c_mon->Modified();
}
