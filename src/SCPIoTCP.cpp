/*
 * SCPIoTCP Implementation: Base Class for SCPI communication with some device connected via TCP
 *
 *  Created on: 20.04.2016
 *      Author: D.Schimansky
 */


#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<termios.h>
#include<stdlib.h>
#include "malloc.h"
#include <sys/ioctl.h>
#include<iostream>
#include<sstream>
#include<list>
#include<math.h>
#include <stdarg.h>

#include "SCPIoTCP.h"

using namespace std;


SCPIoTCP::SCPIoTCP(const char *host, short unsigned port, char cmd_delim, char reply_delim)
	:SCPIoUART("", 0, cmd_delim, reply_delim)
{
	m_host = host;		
	m_port = port;
	Connect();	
}

SCPIoTCP::~SCPIoTCP()
{

}

void SCPIoTCP::Connect(){
	struct sockaddr_in serv_addr;
	struct hostent *server;
	if(m_fd>=0){
		printf("Closing socket..\n");
		close(m_fd);
	}

	m_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (m_fd < 0) {
		perror("ERROR opening socket");
		return;
	}

	server = gethostbyname(m_host);

	if (server == NULL) {
		fprintf(stderr,"ERROR, host %s not found\n",m_host);
		m_fd=-1;
		return;
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, 
	 (char *)&serv_addr.sin_addr.s_addr,
	 server->h_length);

	serv_addr.sin_port = htons(m_port);
	if (connect(m_fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
		printf("Error opening socket to %s:%d. Port seems to be closed.\n",m_host,m_port);
		close(m_fd);
		m_fd=-1;
		return;
	}
	
	printf("Connected to host %s  ->  %s on port %d\n",m_host,inet_ntoa(serv_addr.sin_addr),m_port);
	return;
}

